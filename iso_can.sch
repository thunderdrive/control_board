EESchema Schematic File Version 2
LIBS:control_board-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:switches
LIBS:ThunderDrive
LIBS:ftdi
LIBS:control_board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "Isolated CAN interface"
Date "2017-03-06"
Rev "1"
Comp "ThunderDrive"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ISO1050 U701
U 1 1 58BF7AF6
P 5550 3200
F 0 "U701" H 5550 3550 50  0000 C CNN
F 1 "ISO1050" H 5550 2850 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5550 2850 50  0001 C CIN
F 3 "" H 5550 3200 50  0000 C CNN
	1    5550 3200
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR016
U 1 1 58BECD37
P 4700 2850
F 0 "#PWR016" H 4700 2700 50  0001 C CNN
F 1 "+3.3V" H 4700 2990 50  0000 C CNN
F 2 "" H 4700 2850 50  0000 C CNN
F 3 "" H 4700 2850 50  0000 C CNN
	1    4700 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 58BECD4D
P 4700 3900
F 0 "#PWR017" H 4700 3650 50  0001 C CNN
F 1 "GND" H 4700 3750 50  0000 C CNN
F 2 "" H 4700 3900 50  0000 C CNN
F 3 "" H 4700 3900 50  0000 C CNN
	1    4700 3900
	1    0    0    -1  
$EndComp
$Comp
L C C702
U 1 1 58BECD63
P 4700 3550
F 0 "C702" H 4725 3650 50  0000 L CNN
F 1 "100n" H 4725 3450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4738 3400 50  0001 C CNN
F 3 "" H 4700 3550 50  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L C C701
U 1 1 58BECD88
P 6600 3450
F 0 "C701" H 6625 3550 50  0000 L CNN
F 1 "100n" H 6625 3350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6638 3300 50  0001 C CNN
F 3 "" H 6600 3450 50  0000 C CNN
	1    6600 3450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P701
U 1 1 58BECE1E
P 7850 3150
F 0 "P701" H 7850 3400 50  0000 C CNN
F 1 "CAN" V 7950 3150 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Pico-EZmate_04x1.20mm_Angled" H 7850 3150 50  0001 C CNN
F 3 "" H 7850 3150 50  0000 C CNN
	1    7850 3150
	1    0    0    1   
$EndComp
Wire Wire Line
	6600 2800 6600 3300
Connection ~ 6600 3000
Wire Wire Line
	5950 3400 6050 3400
Wire Wire Line
	6050 3400 6050 3700
Wire Wire Line
	6050 3700 7400 3700
Wire Wire Line
	7400 3700 7400 3300
Wire Wire Line
	7400 3300 7650 3300
Wire Wire Line
	6600 3600 6600 3850
Connection ~ 6600 3700
Wire Wire Line
	5950 3150 6250 3150
Wire Wire Line
	6250 3150 6250 3100
Wire Wire Line
	6250 3100 7650 3100
Wire Wire Line
	5950 3250 6350 3250
Wire Wire Line
	6350 3250 6350 3200
Wire Wire Line
	6350 3200 7650 3200
Wire Wire Line
	4700 3700 4700 3900
Wire Wire Line
	4700 3800 5000 3800
Wire Wire Line
	5000 3800 5000 3400
Wire Wire Line
	5000 3400 5150 3400
Connection ~ 4700 3800
Wire Wire Line
	5150 3000 4700 3000
Wire Wire Line
	4700 2850 4700 3400
Connection ~ 4700 3000
Wire Wire Line
	5150 3150 4450 3150
Wire Wire Line
	4450 3250 5150 3250
Text HLabel 4450 3150 0    60   Input ~ 0
RXD
Text HLabel 4450 3250 0    60   Output ~ 0
TXD
$Comp
L PWR_FLAG #FLG018
U 1 1 58C021BF
P 6600 2800
F 0 "#FLG018" H 6600 2895 50  0001 C CNN
F 1 "PWR_FLAG" H 6600 2980 50  0000 C CNN
F 2 "" H 6600 2800 50  0000 C CNN
F 3 "" H 6600 2800 50  0000 C CNN
	1    6600 2800
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG019
U 1 1 58C021FD
P 6600 3850
F 0 "#FLG019" H 6600 3945 50  0001 C CNN
F 1 "PWR_FLAG" H 6600 4030 50  0000 C CNN
F 2 "" H 6600 3850 50  0000 C CNN
F 3 "" H 6600 3850 50  0000 C CNN
	1    6600 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 3000 7650 3000
$EndSCHEMATC
