EESchema Schematic File Version 2
LIBS:control_board-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:switches
LIBS:ThunderDrive
LIBS:ftdi
LIBS:control_board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Connector to logic board"
Date "2017-03-20"
Rev "1"
Comp "ThunderDrive"
Comment1 "har-flex 26 pin straight"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L har-flex_THR_26 J601
U 1 1 58D2A65D
P 4100 1600
F 0 "J601" H 4100 2300 50  0000 C CNN
F 1 "har-flex_THR_26" V 4100 1600 50  0000 C CNN
F 2 "ThunderDrive:har-flex-THR-26pin-male-right" H 4100 450 50  0001 C CNN
F 3 "" H 4100 450 50  0001 C CNN
	1    4100 1600
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR08
U 1 1 58D2A664
P 5800 3750
F 0 "#PWR08" H 5800 3600 50  0001 C CNN
F 1 "+3.3V" H 5800 3890 50  0000 C CNN
F 2 "" H 5800 3750 50  0001 C CNN
F 3 "" H 5800 3750 50  0001 C CNN
	1    5800 3750
	1    0    0    -1  
$EndComp
$Comp
L +3.3VADC #PWR09
U 1 1 58D2A670
P 6800 3700
F 0 "#PWR09" H 6950 3650 50  0001 C CNN
F 1 "+3.3VADC" H 6800 3800 50  0000 C CNN
F 2 "" H 6800 3700 50  0001 C CNN
F 3 "" H 6800 3700 50  0001 C CNN
	1    6800 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1400 4350 1400
Wire Wire Line
	4350 1200 4800 1200
$Comp
L GNDA #PWR010
U 1 1 58D2A67A
P 4600 4300
F 0 "#PWR010" H 4600 4050 50  0001 C CNN
F 1 "GNDA" H 4600 4150 50  0000 C CNN
F 2 "" H 4600 4300 50  0001 C CNN
F 3 "" H 4600 4300 50  0001 C CNN
	1    4600 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1600 4350 1600
Wire Wire Line
	4800 1800 4350 1800
Wire Wire Line
	4800 1900 4350 1900
Wire Wire Line
	4800 2000 4350 2000
Wire Wire Line
	4800 2200 4350 2200
Wire Wire Line
	4350 1000 4800 1000
Text HLabel 4800 1000 2    60   Input ~ 0
CLOCK
Text HLabel 4800 1800 2    60   Output ~ 0
TEMP_A
Text HLabel 4800 1200 2    60   Output ~ 0
DATA_A
Text HLabel 4850 3300 2    60   Input ~ 0
HS_PWM_A
Text HLabel 4850 3000 2    60   Input ~ 0
LS_PWM_A
Text HLabel 4850 3400 2    60   Input ~ 0
HS_PWM_B
Text HLabel 4850 3100 2    60   Input ~ 0
LS_PWM_B
Text HLabel 4800 1900 2    60   Output ~ 0
TEMP_B
Text HLabel 4800 1400 2    60   Output ~ 0
DATA_B
Text HLabel 4850 3500 2    60   Input ~ 0
HS_PWM_C
Text HLabel 4850 3200 2    60   Input ~ 0
LS_PWM_C
Text HLabel 4800 2000 2    60   Output ~ 0
TEMP_C
Text HLabel 4800 1600 2    60   Output ~ 0
DATA_C
Text HLabel 4800 2200 2    60   Input ~ 0
VDC_EXT
NoConn ~ 3850 1000
NoConn ~ 3850 1300
NoConn ~ 3850 1400
NoConn ~ 3850 1800
NoConn ~ 3850 1900
NoConn ~ 3850 2100
NoConn ~ 3850 2200
$Comp
L PWR_FLAG #FLG011
U 1 1 58D82537
P 6300 3750
F 0 "#FLG011" H 6300 3825 50  0001 C CNN
F 1 "PWR_FLAG" H 6300 3900 50  0000 C CNN
F 2 "" H 6300 3750 50  0001 C CNN
F 3 "" H 6300 3750 50  0001 C CNN
	1    6300 3750
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG012
U 1 1 58D8254C
P 7300 3750
F 0 "#FLG012" H 7300 3825 50  0001 C CNN
F 1 "PWR_FLAG" H 7300 3900 50  0000 C CNN
F 2 "" H 7300 3750 50  0001 C CNN
F 3 "" H 7300 3750 50  0001 C CNN
	1    7300 3750
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG013
U 1 1 58D82594
P 4950 4300
F 0 "#FLG013" H 4950 4375 50  0001 C CNN
F 1 "PWR_FLAG" H 4950 4450 50  0000 C CNN
F 2 "" H 4950 4300 50  0001 C CNN
F 3 "" H 4950 4300 50  0001 C CNN
	1    4950 4300
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG014
U 1 1 58D825A2
P 3300 4300
F 0 "#FLG014" H 3300 4375 50  0001 C CNN
F 1 "PWR_FLAG" H 3300 4450 50  0000 C CNN
F 2 "" H 3300 4300 50  0001 C CNN
F 3 "" H 3300 4300 50  0001 C CNN
	1    3300 4300
	-1   0    0    1   
$EndComp
Text HLabel 4850 3600 2    60   Input ~ 0
BRAKE_PWM
$Comp
L har-flex_THR_26 J602
U 1 1 58E84951
P 4100 3600
F 0 "J602" H 4100 4300 50  0000 C CNN
F 1 "har-flex_THR_26" V 4100 3600 50  0000 C CNN
F 2 "ThunderDrive:har-flex-THR-26pin-male-right" H 4100 2450 50  0001 C CNN
F 3 "" H 4100 2450 50  0001 C CNN
	1    4100 3600
	1    0    0    -1  
$EndComp
NoConn ~ 3850 1100
NoConn ~ 3850 1200
NoConn ~ 3850 1500
NoConn ~ 3850 1600
NoConn ~ 3850 1700
NoConn ~ 3850 2000
NoConn ~ 4350 1100
NoConn ~ 4350 1300
NoConn ~ 4350 1500
NoConn ~ 4350 1700
NoConn ~ 4350 2100
Wire Wire Line
	4350 3000 4850 3000
Wire Wire Line
	4350 3100 4850 3100
Wire Wire Line
	4350 3200 4850 3200
Wire Wire Line
	4350 3300 4850 3300
Wire Wire Line
	4350 3400 4850 3400
Wire Wire Line
	4350 3500 4850 3500
Wire Wire Line
	4850 3600 4350 3600
$Comp
L GND #PWR015
U 1 1 58E850E4
P 3700 4300
F 0 "#PWR015" H 3700 4050 50  0001 C CNN
F 1 "GND" H 3700 4150 50  0000 C CNN
F 2 "" H 3700 4300 50  0001 C CNN
F 3 "" H 3700 4300 50  0001 C CNN
	1    3700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4200 3850 4200
Wire Wire Line
	3700 4200 3700 4300
NoConn ~ 3850 3000
NoConn ~ 3850 3100
NoConn ~ 3850 3200
NoConn ~ 3850 3300
NoConn ~ 3850 3400
NoConn ~ 3850 3500
NoConn ~ 3850 3600
NoConn ~ 3850 3700
NoConn ~ 3850 3800
NoConn ~ 3850 3900
NoConn ~ 3850 4000
NoConn ~ 3850 4100
Wire Wire Line
	3300 4200 3300 4300
Connection ~ 3700 4200
Wire Wire Line
	4350 4200 4950 4200
Wire Wire Line
	4600 4200 4600 4300
Wire Wire Line
	4950 4200 4950 4300
Connection ~ 4600 4200
Wire Wire Line
	5800 3750 5800 4000
Wire Wire Line
	4350 4000 6300 4000
Wire Wire Line
	6300 4000 6300 3750
Connection ~ 5800 4000
Wire Wire Line
	6800 3700 6800 4100
Wire Wire Line
	4350 4100 7300 4100
Wire Wire Line
	7300 4100 7300 3750
Connection ~ 6800 4100
Wire Wire Line
	4350 3700 4850 3700
Text HLabel 4850 3700 2    60   Output ~ 0
GATE_ENABLE
Wire Wire Line
	4350 3800 4850 3800
Wire Wire Line
	4350 3900 4850 3900
Text HLabel 4850 3800 2    60   Output ~ 0
AIN1
Text HLabel 4850 3900 2    60   Output ~ 0
AIN2
$EndSCHEMATC
