# README #

Monster MKII control board KiCAD files 

### What is this repository for? ###

* The design files for the control board of the Monster MKII inverter.

### How do I get set up? ###

* Download and install KiCAD Ver. 4.0.5+
* clone this repo
* open the control_board.pro file in KiCAD